$(document).ready(function(){
  var form = $("#mainForm");
  autoCreate(form); 

  $("#list").on("click",".entry",function(){
    //          var msg = $(this).attr("comments");
    $("#msg").html(msg);
    $('#Modal').modal('show');
  });

  $("#submit").click(function(event){
    event.preventDefault();
    console.log("test");
    var elements = form;
    var url = "update.php";
    var scroll = true;
    submit(url,elements,scroll);
  });

  updateList();
});


function updateList(){
  var url = "get.php";
  $.getJSON( url, function( data ) {
    $("#list").html("");
    for( i in data){
      var date = data[i].date;

      date = formatAMPM(date);
      var name = data[i].name;
      var phone = data[i].phone;
      //           var comments = data[i].comments;
      $("#list").append("<tr class='entry'>"+
        "<td>"+date+"</td>"+
        "<td>"+name+"</td>"+
        "<td>"+phone+"</td>"+
      "</tr>");
    }
  });
}
