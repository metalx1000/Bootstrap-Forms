      <div class="container">
        <h2>Example Table Form</h2>
        <form id="mainForm">
          <div class="form-group">
            <div class="row">
              <div class="col-sm-6">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
              </div>
              <div class="col-sm-6">
                <label for="phone">Phone:</label>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="Enter Phone Number">
              </div>
            </div>
            <br>

            <div class="row">
              <div class="col-lg-12">
                <button id="submit" class="btn btn-primary btn-block btn-lg">Submit</button>
              </div>
            </div>
              <hr>
          </div>
        </form>


      </div>
