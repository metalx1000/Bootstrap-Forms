<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Example Table Form</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="libs/jquery.min.js"></script>
    <script src="libs/bootstrap.min.js"></script>
    <script src="libs/forms.js"></script>
    <script src="js/main.js"></script>

    <body>
      <?php include("form.php");?>
      <?php include("cells.php");?>
      <?php include("modal.php");?>
    </body>
  </html>


